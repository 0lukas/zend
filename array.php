<?php

// Range (default step = 1)
$range = range(1.1, 4.2);
var_dump($range); // 1.1, 2.1, 3.1, 4.1

// Comparing
$a = [0 => 1, 1 => 0];
$b = [1 => 0, 0 => 1];
var_dump([$a == $b, $a === $b]); // true, false

// Diff
$a = [5, 10, 15];
$b = [7, 12, 17];
$c = [5, 7, 12];
var_dump(array_diff($a, $b, $c)); // 10, 15 (unique in first array)
// array_diff checks only value
// array_diff_assoc checks key and value

// Intersect (inverse of array_diff)
$a = [5, 10, 15];
$b = [5, 7, 10];
var_dump(array_intersect($a, $b)); // 5, 10

// Shift, unshift, pop, pust
$a = [7, 8];
var_dump(array_shift($a)); // 7

array_unshift($a, 9); 
var_dump($a); // 9, 8

var_dump(array_pop($a)); // 8
var_dump($a); // 9

array_push($a, 5);
var_dump($a); // 9, 5

// Sorting:
// sort - alphabetically (1, 10, 11, 2 .. 9)
// rsort - reverse alphabetically
// asort, arsort - associative (reverse) alphabetically
// ksort - key sort
// krsort - reverse key sort
// usort - user defined comparisoni function
// shuffle - pseudo random
// natsort - natural sorting (1, 2, .. 10, 11)

$a = ['a' => 5, 'b' => 4];
sort($a);
// $a = [4, 5];
asort($a);
// $a = ['b' => 4, 'a' => 5];

// SPL (standart PHP library)
$a = ['hp' => 100, 'money' => 50];
$o = new ArrayObject($a);

var_dump($o);
$o->asort();
var_dump($o);

$o->setFlags(ArrayObject::ARRAY_AS_PROPS);
var_dump($o->money);
