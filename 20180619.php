<?php

function dumpValues(string $title, array $array) {
    printf("%s %s\n", $title, implode(', ', $array));
}

// ldap - Lightweight Directory Access Protocol 
// PHP default libai

// Throwable / exception / error
/*
  |- Exception implements Throwable
       |- …
    |- Error implements Throwable
        |- TypeError extends Error
        |- ParseError extends Error
        |- ArithmeticError extends Error
            |- DivisionByZeroError extends ArithmeticError
        |- AssertionError extends Error
*/

// If you want to catch both exceptions and errors, catch \Throwable. If you want to only catch exceptions, then catch \Exception. And if you want to only catch errors, use catch \Error

try {
    throw new Exception('GG');    
} catch (Throwable $t) {
    var_dump('PHP7: ' . $t->getMessage());
} catch (Exception $e) {
    var_dump('PHP5: ' . $e->getMessage());
}

// Sort:
// php.net: Be careful when sorting arrays with mixed types values because sort() can produce unpredictable results.

$array = ['a', 0, 'a', 0, 1, 'b', '0', 2];
dumpValues('Original:      ', $array);
sort($array);
dumpValues('Sort (regular):', $array);
sort($array, SORT_NUMERIC);
dumpValues('Sort (numeric):', $array);
sort($array, SORT_STRING);
dumpValues('Sort (string): ', $array);
natsort($array);
dumpValues('Natsort:       ', $array);

// StdObject:
$random = new stdClass;
$random->tadcka = 'kietas';
var_dump($random);

unset($random);

$random['tadcka'] = 'kietas';
var_dump((object)$random);

/* Symfony stdClass:
 * Tests/*
 * src/Symfony/Bundle/FrameworkBundle/DependencyInjection/FrameworkExtension.php:
 *  $connectionDefinition = new Definition(\stdClass::class);
 */

// Php.ini:
/* session.cookie_lifetime
 * session.name
 * ...
 * session.*
 * 
 * 
 */

 // Iterators:
 // http://php.net/manual/en/class.iterator.php
 // http://php.net/manual/en/class.arrayiterator.php
