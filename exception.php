<?php

function tryReturn() {
    try {
        return 0;
    } finally {
        echo 'Code execution after return statement.';
    }
    
    echo 'This text will never be printed.';
}

function tryUncatchedException() {
    try {
        throw new Exception('This exception is not catched.');
    } catch (RuntimeException $e) {
        echo $e->getMessage();
    } finally {
        echo 'This text is printed even after uncatched exception.';
    }

    echo 'But this text is will be never printed.';
}

tryReturn();

echo "\n\n! ! ! Exception below is expected.\n\n";

tryUncatchedException();

