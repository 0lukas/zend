# Zend preparation
1. (!2) Arrays (enumerated, associative, multi-dimensional, iteration, functions, SPL/Objects) - 2018-06-06
1. (!1) PHP Basics (syntax, operators, variables, control structures, language constructs & functions, constants, namespaces, extensions, configuration, performance) - 2018-06-08
1. (!2) Functions (syntax, arguments, variables, references, returns, variable scope, closures, declarations)  - 2018-06-13
1. (!3) Data Format & Types (xml basics, xml extension, simplexml, dom, soap, rest, json & ajax, date & time)  - 2018-06-14
1. (!2) String & Patterns (quoting, here(now)doc, matching, extracting, searching, replacing, formattings, PCRE, encoding) - 2018-06-15
1. (!1) OOP (instantiation, instance methods & properties, class definition, modifiers, exceptions, static methods, autoload, reflection, type hinting, constants, bindings, magic, SPL, traits) - 2018-06-20
1. (!1) Security (configuration, session, XSS, sql ijnection, remote site injection, email injection, input filtering, escape output, password hashing, file uploads, data storage, ssl) - 2018-06-22
1. (!3) Database (sql, joins, analyzing queries, prepared statements, transactions, PDO) - 2018-06-25
1. (!2) Web Features (sessions, forms, get and post data, file uploads, cookies, http headers and codes) - 2018-06-27
1. (!3) Streams, Networking, & I/O (files, FS functions, streams, contexts, reading, writing) - 2018-06-28
1. (!3) Error Handling (levels, display, user-defined errors, exception handling, error class) - 2018-07-04
